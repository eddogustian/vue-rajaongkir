# vue-rajaongkir

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### Langkah 1 - Installasi Tailwind CSS
Sekarang, kita akan belajar bagaimana cara mengintegrasikan project Vue Js dengan Tailwind CSS, silahkan jalankan perintah di bawah ini di dalam project Vue Js:

```
npm install -D tailwindcss@npm:@tailwindcss/postcss7-compat @tailwindcss/postcss7-compat postcss@^7 autoprefixer@^9
```

### Langkah 2 - Konfigurasi Tailwind CSS di Vue Js
Setelah proses installasi selesai, sekarang kita lanjutkan untuk melakukan konfigurasi Tailwind CSS di dalam project Vue Js, silahkan jalankan perintah berikut ini :

``` 
npx tailwindcss init -p
```

Perintah di atas digunakan untuk melakukan generate file konfigurasi dari Tailwind, yaitu : tailwind.config.js dan postcss.config.js.